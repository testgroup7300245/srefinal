const Prometheus = require('prom-client')

const http_request_counter = new Prometheus.Counter({
    name: 'myapp_http_request_count',
    help: 'Count of HTTP requests made to my app',
    labelNames: ['method', 'route', 'statusCode', 'msg'],
});

module.exports = http_request_counter
