const Prometheus = require('prom-client')

const http_request_duration_milliseconds = new Prometheus.Histogram({
    name: 'myapp_http_request_duration_milliseconds',
    help: 'Duration of HTTP requests in milliseconds.',
    labelNames: ['method', 'route', 'code'],
    buckets: [1, 2, 3, 4, 5, 10, 25, 50, 100, 250, 500, 1000],
})

module.exports = http_request_duration_milliseconds
